defmodule Airbnb do
  def ccm, do: %Geo.Polygon{coordinates: [[{19.284829, -99.1697213}]]}
  def csf, do: %Geo.Polygon{coordinates: [[{19.3592502,-99.2609819}]]}
  def example1 do
    %Geo.Polygon{coordinates: [[
      {19.27947808173419, -99.1584565661355},
      {19.29048143661357, -99.14978149565974},
      {19.302986793558276, -99.15537799284813},
      {19.30448825987522, -99.16965199540661},
      {19.29348321909907, -99.17832713620781},
      {19.28097839809594, -99.17272820469735}
    ]]}
  end

  def precioProm_Enum() do
    File.stream!("listings-2.csv")
    |> CSV.decode!(headers: true)
    |> Enum.group_by(fn row -> row["neighbourhood_cleansed"] end)
    |> Enum.map(fn {a,b} -> {a, b|> Enum.reduce({0,0},fn row, {suma, cuenta} -> {(row["price"] |> String.replace("$","") |> String.replace(",","") |> String.to_float()) + suma, cuenta + 1} end)} end)
    |> Enum.map(fn {a, {b,c}} -> ["Precio promedio " <> a] ++ Kernel./(b,c) end)
  end

  def precioProm_Flow() do
    File.stream!("listings-2.csv")
    |> CSV.decode!(headers: true)
    |> Flow.from_enumerable()
    |> Flow.partition(key: fn row -> row["neighbourhood_cleansed"] end) 
    |> Flow.group_by(fn row -> row["neighbourhood_cleansed"] end)
    |> Flow.map(fn {a,b} -> {a, b|> Enum.reduce({0,0},fn row, {suma, cuenta} -> {(row["price"] |> String.replace("$","") |> String.replace(",","") |> String.to_float()) + suma, cuenta + 1} end)} end)
    |> Flow.map(fn {a, {b,c}} -> ["Precio promedio " <> a] ++ Kernel./(b,c) end)
    |> Enum.to_list 
  end

  def capTotal_Enum() do
    File.stream!("listings-2.csv")
    |> CSV.decode!(headers: true)
    |> Enum.group_by(fn row -> row["neighbourhood_cleansed"] end)
    |> Enum.map(fn {a,b} -> {a, b|> Enum.reduce(0,fn row, acc -> (row["accommodates"] |> String.to_integer()) + acc end)} end)
  end

  def capTotal_Flow() do
    File.stream!("listings-2.csv")
    |> CSV.decode!(headers: true)
    |> Flow.from_enumerable()
    |> Flow.partition(key: fn row -> row["neighbourhood_cleansed"] end) 
    |> Flow.group_by(fn row -> row["neighbourhood_cleansed"] end)
    |> Flow.map(fn {a,b} -> {a, b|> Enum.reduce(0,fn row, acc -> (row["accommodates"] |> String.to_integer()) + acc end)} end)
    |> Enum.to_list 
  end

  def capModa_Enum() do
    File.stream!("listings-2.csv")
    |> CSV.decode!(headers: true)
    |> Enum.group_by(fn row -> row["neighbourhood_cleansed"] end)
    |> Enum.map(fn {a,b} -> {a, b|> Enum.map(fn row-> row["accommodates"] |> String.to_integer() end)} end)
    |> Enum.map(fn {a,b} -> {a, mode(b)} end)
  end

  def capModa_Flow() do
    File.stream!("listings-2.csv")
    |> CSV.decode!(headers: true)
    |> Flow.from_enumerable()
    |> Flow.partition(key: fn row -> row["neighbourhood_cleansed"] end) 
    |> Flow.group_by(fn row -> row["neighbourhood_cleansed"] end)
    |> Flow.map(fn {a,b} -> {a, b|> Enum.map(fn row-> row["accommodates"] |> String.to_integer() end)} end)
    |> Enum.map(fn {a,b} -> {a, mode(b)} end)
  end

  def mode(list) do
    gb = Enum.group_by(list, &(&1))
    max = Enum.map(gb, fn {_,val} -> length(val) end) |> Enum.max
    for {key,val} <- gb, length(val)==max, do: key
  end

  def count_Enum() do
    File.stream!("listings-2.csv")
    |> CSV.decode!(headers: true)
    |> Enum.group_by(fn row -> row["neighbourhood_cleansed"] end)
    |> Enum.map(fn {a,b} -> {a, b|> Enum.group_by(fn row -> row["property_type"] end) } end)
    |> Enum.map(fn {a,b} -> {a, Enum.map(b, fn {k,c} -> {k, length(c)} end)} end)
  end

  def count_Flow() do
    File.stream!("listings-2.csv")
    |> CSV.decode!(headers: true)
    |> Flow.from_enumerable()
    |> Flow.partition(key: fn row -> row["neighbourhood_cleansed"] end) 
    |> Flow.group_by(fn row -> row["neighbourhood_cleansed"] end)
    |> Flow.map(fn {a,b} -> {a, b|> Enum.group_by(fn row -> row["property_type"] end) } end)
    |> Enum.map(fn {a,b} -> {a, Enum.map(b, fn {k,c} -> {k, length(c)} end)} end)
  end

  #Benchee.run( %{ "eager" => &Airbnb.precioProm_Enum/0, "lazy" => &Airbnb.precioProm_Flow/0 }, memory_time: 2 )
  
  #Lo que se debe de poner en mix.exs
  #:benchee, "~> 1.0", only: :dev} 
end